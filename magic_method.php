<?php
/**
 * Date: 10/24/2016
 * Time: 12:16 PM
 */
class StudentInfo{

    public $justOneVariable= "Hello BiTM<br>";

    public function justOneMethod()
    {
        echo "Hello World!<br>";
    }

    public function __construct()
    {
        echo $this->justOneVariable;
    }

    public function __destruct()
    {
        echo "Good Bye!<br>";
    }
}

$justOneObject = new StudentInfo;

$justOneObject->justOneMethod();




