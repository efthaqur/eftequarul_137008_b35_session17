<?php
/**
 * Date: 10/24/2016
 * Time: 10:06 AM
 */

    class StudentInfo{

        public $std_id="";
        public $std_name="";
        public $std_cgpa=0.00;

        public function set_id($std_id)
        {
            $this->std_id = $std_id;


        }

        public function set_name($std_name)
        {
            $this->std_name=$std_name;
        }

        public function set_cgpa($std_cgpa)
        {
            $this->std_cgpa = $std_cgpa;
        }

        public function get_std_id()
        {
            return $this->std_id;
        }

        public function get_std_name()
        {
            return $this->std_name;
        }

        public function get_std_cgpa()
        {
            return $this->std_cgpa;
        }

        public function showDetails()
        {
            echo "Student ID: ".$this->std_id."<br>";
            echo "Student Name: ".$this->std_name."<br>";
            echo "Student CGPA: ".$this->std_cgpa."<br>";
        }

    }

$obj = new StudentInfo;

$obj->set_id("SEIP137008");
$obj->set_name("Efthaqur Alam");
$obj->set_cgpa(3.50);

echo $obj->get_std_id()."<br>";
echo $obj->get_std_name()."<br>";
echo $obj->get_std_cgpa()."<br>";

echo "<br>";
$obj->showDetails();






echo "<br><br><br>";
echo "::::::::::::::::::::::::: var_dump() :::::::::::::::::::::::::::::::::::<br>";
echo "<pre>";
var_dump($obj);
echo "</pre>";
echo ":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::<br>";







