<?php
	
	class PrimeNumber{

		public $number;
		public $onlyPrime;

		public function __construct($num){
			$this->number=$num;

		}

		public function primeCheck(){
			
			for($m=1; $m<=$this->number; $m++){
				if($m==1)
					continue;
				$isPrime=true;
				for($n=2; $n<=ceil($m/2); $n++){
					if($m%$n==0){
						$isPrime=false;
						break;
					}
				}
				if($isPrime){
					if($isPrime) {
						$this->onlyPrime=$m;
						echo "Prime Number: ".$this->onlyPrime." <br>";
					}
				}
			}

			}
		}

	$primeObj = new PrimeNumber(100);
	$primeObj->primeCheck();

?>	